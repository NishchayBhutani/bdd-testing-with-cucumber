Feature: Evaluate discount percentage

  Scenario: Verify 10% discount
    Given Execute DiscountService
    When I enter amount as 5001
    Then I should get ten percent discount

  Scenario: Verify 15% discount
  	Given Execute DiscountService
  	When I enter amount as 10001
  	Then I should get fifteen percent discount
  	
  Scenario: Verify no offer
  	Given Execute DiscountService
  	When I enter amount as 4000
  	Then I should get no discount